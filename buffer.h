#define once
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

struct buffer16 {
  uint16_t *buffer;
  uint16_t size;
};

struct buffer8 {
  uint8_t *buffer;
  uint16_t size;
};

void buffer16Alloc0d(struct buffer16 *var, uint16_t bufferSize);
void buffer16Alloc1d(struct buffer16 **var, uint16_t bufferQuant, uint16_t bufferSize);
void buffer16AllocMulti1d(struct buffer16 **var, uint16_t bufferQuant, uint16_t *bufferSize);
void buffer16Alloc2d(struct buffer16 ***var, uint16_t bufferQM, uint16_t bufferQN, uint16_t bufferSize);
void printBuffer160d(struct buffer16 *var);
void printBuffer161d(struct buffer16 **var, uint16_t q);
void printBuffer162d(struct buffer16 ***var, uint16_t m, uint16_t n);
void freeBuffer160d(struct buffer16 *var);
void freeBuffer161d(struct buffer16 **var, uint16_t q);
void freeBuffer162d(struct buffer16 ***var, uint16_t m, uint16_t n);

void buffer8Alloc0d(struct buffer8 *var, uint16_t bufferSize);
void buffer8Alloc1d(struct buffer8 **var, uint16_t bufferQuant, uint16_t bufferSize);
void buffer8AllocMulti1d(struct buffer8 **var, uint16_t bufferQuant, uint16_t *bufferSize);
void buffer8Alloc2d(struct buffer8 ***var, uint16_t bufferQM, uint16_t bufferQN, uint16_t bufferSize);
void printBuffer80d(struct buffer8 *var);
void printBuffer81d(struct buffer8 **var, uint16_t q);
void printBuffer82d(struct buffer8 ***var, uint16_t m, uint16_t n);
void freeBuffer80d(struct buffer8 *var);
void freeBuffer81d(struct buffer8 **var, uint16_t q);
void freeBuffer82d(struct buffer8 ***var, uint16_t m, uint16_t n);

void buffer16Alloc0d(struct buffer16 *var,
		   uint16_t bufferSize) {
  var->buffer = malloc(sizeof(uint16_t) * bufferSize);
  var->size = bufferSize;
}

void buffer16Alloc1d(struct buffer16 **var,
		   uint16_t bufferQuant, uint16_t bufferSize) {
  
  *var = malloc(sizeof(struct buffer16) * bufferQuant);
  do {
    (*var)[bufferQuant - 1].buffer = malloc(sizeof(uint16_t) * bufferSize);
    (*var)[bufferQuant - 1].size = bufferSize;
  }
  while(--bufferQuant);
}

void buffer16AllocMulti1d(struct buffer16 **var,
			uint16_t bufferQuant, uint16_t *bufferSize) {
  
  *var = malloc(sizeof(struct buffer16) * bufferQuant);
  do {
    (*var)[bufferQuant - 1].buffer = malloc(sizeof(uint16_t) * bufferSize[bufferQuant - 1]);
    (*var)[bufferQuant - 1].size = bufferSize[bufferQuant - 1];
  }
  while(--bufferQuant);
}

void buffer16Alloc2d(struct buffer16 ***var, uint16_t bufferQM,
		   uint16_t bufferQN, uint16_t bufferSize) {
  uint16_t i, j;
  var = malloc(sizeof(struct buffer **) * bufferQM);
  for(i = 0; i < bufferQM; ++i)
    var[i] = malloc(sizeof(struct buffer *) * bufferQN);
  
  for(i = 0; i < bufferQM; ++i) {
    for(j = 0; j < bufferQN; ++j) {
      var[i][j]->buffer = malloc(sizeof(uint16_t) * bufferSize);
      var[i][j]->size = bufferSize;
    }
  }
}

void printBuffer160d(struct buffer16 *var) {
  printf("buffer: --------------\n");
  for(uint16_t i = 0; i < var->size; ++i) {
    printf("%d %04X\n", i, var->buffer[i]);
  }
  printf("\n----------------------\n");
}

void printBuffer161d(struct buffer16 **var, uint16_t q) {
  printf("buffer: --------------\n");
  for(uint16_t i = 0; i < q; ++i) {
    for(uint16_t j = 0; j < (*var)[i].size; ++j)
      printf("%d %d %04X\n", i, j, (*var)[i].buffer[j]);
  }
  printf("----------------------\n");  
}

void printBuffer162d(struct buffer16 ***var, uint16_t m, uint16_t n) {
  printf("buffer: --------------\n");
  for(uint16_t i = 0; i < m; ++i)
    for(uint16_t j = 0; j < n; ++j)
      for(uint16_t y = 0; y < var[i][j]->size; ++y)
	printf("%d %d %d %04X\n", i, j, y, var[i][j]->buffer[y]);
  printf("\n----------------------\n");
}

void freeBuffer160d(struct buffer16 *var) {
  free(var->buffer);
}

void freeBuffer161d(struct buffer16 **var, uint16_t q) {
  do { free((*var)[q - 1].buffer); } while(--q);
  free(*var);
}

void freeBuffer162d(struct buffer16 ***var, uint16_t m, uint16_t n) {
  uint16_t i, j;
  for(i = 0; i < m; ++i)
    for(j = 0; j < n; ++j)
      free(var[i][j]->buffer);

  do { free(&var[m - 1][0]); } while(--m);

  free(var);
}

void buffer8Alloc0d(struct buffer8 *var, uint16_t bufferSize) {
  var->buffer = malloc(sizeof(uint8_t) * bufferSize);
  var->size = bufferSize;
}

void buffer8Alloc1d(struct buffer8 **var,
		   uint16_t bufferQuant, uint16_t bufferSize) {
  
  *var = malloc(sizeof(struct buffer8) * bufferQuant);
  do {
    (*var)[bufferQuant - 1].buffer = malloc(sizeof(uint8_t) * bufferSize);
    (*var)[bufferQuant - 1].size = bufferSize;
  }
  while(--bufferQuant);
}

void buffer8AllocMulti1d(struct buffer8 **var,
			uint16_t bufferQuant, uint16_t *bufferSize) {
  
  *var = malloc(sizeof(struct buffer8) * bufferQuant);
  do {
    (*var)[bufferQuant - 1].buffer = malloc(sizeof(uint8_t) * bufferSize[bufferQuant - 1]);
    (*var)[bufferQuant - 1].size = bufferSize[bufferQuant - 1];
  }
  while(--bufferQuant);
}

void buffer8Alloc2d(struct buffer8 ***var, uint16_t bufferQM,
		   uint16_t bufferQN, uint16_t bufferSize) {
  uint16_t i, j;
  var = malloc(sizeof(struct buffer8 **) * bufferQM);
  for(i = 0; i < bufferQM; ++i)
    var[i] = malloc(sizeof(struct buffer8 *) * bufferQN);
  
  for(i = 0; i < bufferQM; ++i) {
    for(j = 0; j < bufferQN; ++j) {
      var[i][j]->buffer = malloc(sizeof(uint8_t) * bufferSize);
      var[i][j]->size = bufferSize;
    }
  }
}

void printBuffer80d(struct buffer8 *var) {
  printf("buffer: --------------\n");
  for(uint16_t i = 0; i < var->size; ++i) {
    printf("%d %04X\n", i, var->buffer[i]);
  }
  printf("\n----------------------\n");
}

void printBuffer81d(struct buffer8 **var, uint16_t q) {
  printf("buffer: --------------\n");
  for(uint16_t i = 0; i < q; ++i) {
    for(uint16_t j = 0; j < (*var)[i].size; ++j)
      printf("%d %d %04X\n", i, j, (*var)[i].buffer[j]);
  }
  printf("----------------------\n");  
}

void printBuffer82d(struct buffer8 ***var, uint16_t m, uint16_t n) {
  printf("buffer: --------------\n");
  for(uint16_t i = 0; i < m; ++i)
    for(uint16_t j = 0; j < n; ++j)
      for(uint16_t y = 0; y < var[i][j]->size; ++y)
	printf("%d %d %d %04X\n", i, j, y, var[i][j]->buffer[y]);
  printf("\n----------------------\n");
}

void freeBuffer80d(struct buffer8 *var) {
  free(var->buffer);
}

void freeBuffer81d(struct buffer8 **var, uint16_t q) {
  do { free((*var)[q - 1].buffer); } while(--q);
  free(*var);
}

void freeBuffer82d(struct buffer8 ***var, uint16_t m, uint16_t n) {
  uint16_t i, j;
  for(i = 0; i < m; ++i)
    for(j = 0; j < n; ++j)
      free(var[i][j]->buffer);

  do { free(&var[m - 1][0]); } while(--m);

  free(var);
}
