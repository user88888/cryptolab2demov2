#define DEBUG_
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdint.h>
#include "alph.h"
#include "buffer.h"
#include <locale.h>
#include "file.h"

#define FILECIPHERIN "-fcin\0"
#define FILEKEYIN "-fkin\0"
#define FILECIPHEROUT "-fcout\0"
#define FILEKEYOUT "-fkout\0"
#define FILEKEYSIZEOUT "-fksizeout\0"
#define KEYSIZEIN "-ksizein\0"
#define ALPHVAR "-alph\0"
#define FILEDEC "-fdec\0"
#define SETRT "-rt\0"
#define HELP "-h\0"

/*
 * ************************************************************************************
 * */
static uint8_t *fdata;
static uint16_t fsize = 0;
struct buffer16 cipherUTF8;
struct buffer8 cipherdata;
struct buffer16 cmpBuffer;
struct buffer8 keySizeBuffer;
double r_;
double rt_ = 0.0553;
struct buffer8 keyWordFrequency;
struct buffer16 *probKeyBuffer;
struct buffer8 decdata;
uint8_t *kdata;
uint16_t ksize;
struct buffer16 keyUTF8;
struct buffer8 keydata;
struct buffer16 outdataUTF8;
uint8_t keySizeBufferQuant = 20;
uint8_t keysOutBufferSize;
struct alphParams alphpr;
uint8_t ksizein = 0;
/*
 * ************************************************************************************
 * */

/*
 * FUNCTIONS INTERFACES ***************************************************************
 * */
char readFile(char *filename);

uint16_t filelength(FILE *fp);

void findKeySize(struct buffer8 *cipherDataBuffer, struct buffer16 *cmpResultBuffer);

/*
uint16_t findRate(double *rate, double *truerate,
		  struct buffer16 *cmp);
*/

void findKeySymFreq(struct buffer8 *varAlph0d, struct buffer8 *keyWord0d);

void writeFile(uint8_t *buffer, uint16_t size, char *filename);

uint16_t **newMatrix(uint16_t m, uint16_t n);

void freeMatrix(uint16_t **matrix, uint16_t m);

uint8_t cryptoAnalysis(struct buffer8 *var);

/*
void fillKeyBuffer(struct buffer8 *keyBuffer,
		   struct buffer8 *alphBuffer,
		   uint16_t i, uint16_t shift);

void computeKey(struct buffer8 *keyWord0d,
		struct alphabet *alphabetBuffer,
		uint16_t alphabetBufferSize,
		uint16_t rateletter);

void dec(struct buffer16 *key0d, uint16_t alphBufferSize,
	 struct buffer16 *textIn, struct buffer16 *textOut, uint16_t ALPH);


void putStrInBuffer16(struct buffer16 *var, uint16_t str[]);
*/
char cmpArgs(char *arg0, char *arg1);

void interpretator(char *comand, char *data);

/*
 * ***********************************************************************************
 * */

int main(int argc, char **argv) {
  
  if(argc > 1) {
   for(int i = 1; i < argc; i += 2) {
     interpretator(argv[i], argv[i + 1]);
   }
  } else {
   printf("Bad args.\n");
  }

  if(fsize) { free(fdata); }
  if(cipherUTF8.size) { freeBuffer160d(&cipherUTF8); }
  if(cipherdata.size) { freeBuffer80d(&cipherdata); }
  if(cmpBuffer.size) { freeBuffer160d(&cmpBuffer); }
  if(keySizeBuffer.size) { freeBuffer80d(&keySizeBuffer); }
  if(keyWordFrequency.size) { 
    freeBuffer80d(&keyWordFrequency);
    freeBuffer161d(&probKeyBuffer, alphpr.alphS);
  }
  if(decdata.size) { freeBuffer80d(&decdata); }
  if(ksize) { free(kdata); }
  if(keyUTF8.size) { freeBuffer160d(&keyUTF8); }
  if(keydata.size) { freeBuffer80d(&keydata); }
  if(outdataUTF8.size) { freeBuffer160d(&outdataUTF8); }
}

char readFile(char *filename) {
  FILE *fp;
  if((fp = fopen((char *) filename, "r")) == NULL) {
    printf("Can't open file.\n");
    return 0;
  }

  fsize = filelength(fp);
  fdata = malloc(fsize);
  
  if(fdata == NULL) {
    printf("Bad alloc\n");
    return 0;
  }
  
  fread(fdata, 1, fsize, fp);

  fclose(fp);

  return 1;
}

void writeFile(uint8_t *buffer, uint16_t size, char *filename) {
  FILE *fp;
  if((fp = fopen((char *) filename, "w")) == NULL) {
    printf("Can't create file.\n");
    return;
  }
  for(uint16_t i = 0; i < (size * 2) - 1; i += 2) {
    fprintf(fp, "%c%c", buffer[i + 1], buffer[i]);
  }
  fclose(fp);
}

void findKeySize(struct buffer8 *cipherDataBuffer, struct buffer16 *cmpResultBuffer) {
  struct buffer8 cdcp;
  uint16_t i, n;
  uint8_t cp;
  buffer8Alloc0d(&cdcp, cipherDataBuffer->size);
  
  for(i = 0; i < cipherDataBuffer->size; ++i) {
    cdcp.buffer[i] = cipherDataBuffer->buffer[i];
  }
  
  for(i = 0; i < cipherDataBuffer->size - 1; ++i) {
    n = cdcp.size - 1;
    cp = cdcp.buffer[n];
    
    do {
      --n;
      cdcp.buffer[n + 1] = cdcp.buffer[n];
    } while(n != 0);
    
    cdcp.buffer[n] = cp;
    cmpResultBuffer->buffer[i] ^= cmpResultBuffer->buffer[i];
    for(; n < cipherDataBuffer->size; ++n) {
      if(cipherDataBuffer->buffer[n] == cdcp.buffer[n]) {
        cmpResultBuffer->buffer[i] += 1;
      }
    }
  }

  freeBuffer80d(&cdcp);
}

uint16_t **newMatrix(uint16_t m, uint16_t n) {
  uint16_t **matrix = (uint16_t **)malloc(m * sizeof(uint16_t *));
  do {
    matrix[m-1] = (uint16_t *)malloc(n * sizeof(uint16_t));
  } while(--m);
  return matrix;
}

void freeMatrix(uint16_t **matrix, uint16_t m) {
  do { free(matrix[m-1]); } while(--m);
}

uint8_t **newMatrix8(uint16_t m, uint16_t n) {
  uint8_t **matrix = (uint8_t **)malloc(m * sizeof(uint8_t *));
  do {
    matrix[m-1] = (uint8_t *)malloc(n * sizeof(uint8_t));
  } while(--m);
  return matrix;
}

void freeMatrix8(uint8_t **matrix, uint16_t m) {
  do { free(matrix[m-1]); } while(--m);
}

uint8_t cryptoAnalysis(struct buffer8 *data) {
  uint16_t i, j, k, r;
  uint8_t **matrixSymQuant;
  uint8_t *unicsym = (uint8_t *)malloc(sizeof(uint8_t) * data->size);

  for(i = 0; i < data->size; ++i) {
    unicsym[i] = data->buffer[i];
  }
  
  k = 0;
  for(i = 0; i < data->size; ++i) {
    for(j = 0; j < data->size; ++j) {
      if((i == j) || (unicsym[i] == '~')) {
        continue;
      }
      if(unicsym[i] == unicsym[j]) {
        unicsym[j] = '~';
        ++k;
      }
    }
  }

  k = data->size - k;
  matrixSymQuant = newMatrix8(k, 2);

  for(i = 0, j = 0; i < data->size; ++i) {
    if(unicsym[i] != '~') {
      matrixSymQuant[j][0] = unicsym[i];
      matrixSymQuant[j][1] = 0;
      ++j;
    }
  }

  for(i = 0; i < k; ++i) {
    for(j = 0; j < data->size; ++j) {
      if(matrixSymQuant[i][0] == data->buffer[j]) {
        ++matrixSymQuant[i][1];
      }
    }
  }

  j = 0;
  for(i = 0; i < k; ++i) {
    if(matrixSymQuant[i][1] > j) {
      j = matrixSymQuant[i][1];
      r = matrixSymQuant[i][0];
    }
  }

  freeMatrix8(matrixSymQuant, k);
  free(unicsym);
  return r;
}

uint16_t findKeySizeUseFilter(double *rate, double *truerate, struct buffer16 *cmp, 
    struct buffer8 *keySizeBuffer) {

  register uint16_t i, n;
  for(i = 0, n = 0; i < cmp->size ; ++i) {
    if((*rate = (1.0 * cmp->buffer[i]) / cmp->size) >= *truerate) {
      if(n < keySizeBuffer->size) {
        keySizeBuffer->buffer[n++] = i + 1;
      } else {
        return n;
      }
    }
  }
  return n;
}

void fillSplitBuffer(struct buffer8 *keyBuffer,
		   struct buffer8 *alphBuffer, uint16_t i, uint16_t shift) {
  
  for(uint16_t f = 0; i < alphBuffer->size; i += shift, ++f) {
    keyBuffer->buffer[f] = alphBuffer->buffer[i];
  }
}

void findKeySymFreq(struct buffer8 *varAlph0d, struct buffer8 *keyWord0d) {
  
  struct buffer8 *key1d;
  uint16_t i, k, n;
  uint16_t *bsize = malloc(sizeof(uint16_t) * keyWord0d->size);
  
  k = varAlph0d->size / keyWord0d->size;
  i = 0;
  if((n = varAlph0d->size % keyWord0d->size) != 0) {
    n = keyWord0d->size - n;    
    while(i < n) bsize[i++] = k;
    while(i < keyWord0d->size) bsize[i++] = k - 1;
  }
  else {
    while(i < keyWord0d->size) bsize[i++] = k;
  }
  
  buffer8AllocMulti1d(&key1d, keyWord0d->size, bsize);
  
  for(i = 0; i < keyWord0d->size; ++i) {
    fillSplitBuffer(&key1d[i], varAlph0d, i, keyWord0d->size);
  }
  
  for(i = 0; i < keyWord0d->size; ++i) {
    keyWord0d->buffer[i] = cryptoAnalysis(&key1d[i]);
  }

  freeBuffer81d(&key1d, keyWord0d->size);
}

char cmpArgs(char *arg0, char *arg1) {
  while(*arg0 != '\0' || *arg1 != '\0') {
    if(*arg0 != *arg1) {
      return 0;
    }
    ++arg0;
    ++arg1;
  }
  return (*arg0 == *arg1) ? 1 : 0;
}

uint8_t cpDataFrom16To8(struct buffer16 *b16, struct buffer8 *b8) {
  
  if(b16->size != b8->size) {
    return 0;
  }

  for(int i = 0; i < b8->size; ++i) {
    b8->buffer[i] = retAlph8BitNum(alphRu33, b16->buffer[i], 33);
  }

  return 1;
}

void decv2(struct buffer8 *B_IN, struct buffer8 *B_OUT, struct buffer8 *key) {
  register uint16_t i, n;
  for(i = 0, n = 0; i < B_IN->size; ++i) {
    B_OUT->buffer[i] = alphDecRu33[B_IN->buffer[i]][key->buffer[n++]];
    if(n == key->size) n ^= n;
  }
}

void translateDataToOutUTF8(struct alphParams *apr,
                            struct buffer8 *outBuffer,
                            struct buffer16 *outUTF8) {
  register uint16_t i;
  for(i = 0; i < outBuffer->size; ++i) {
    outUTF8->buffer[i] = apr->alphL[outBuffer->buffer[i]].sym->utfcode;
  }
}

void computeProbKeys(struct buffer16 **probKeyBuffer, 
                     struct buffer8 *keyWordFrequency,
                     struct alphParams *apr) {
  register int16_t i, j, s, e;
  for(i = 0; i < apr->alphS; ++i) {
    s = apr->alphF[i].letterPtr->number;
    --s;
    for(j = 0, e = keyWordFrequency->buffer[j]; 
        j < keyWordFrequency->size; 
        ++j, e = keyWordFrequency->buffer[j]) 
    {
      e = e - s;
      if(e < 0) {
        e = apr->alphS + e;
      }

      (*probKeyBuffer)[i].buffer[j] = apr->alphL[e].sym->utfcode;
    }
  }
}

void writeFileProbKeyBuffer(struct buffer16 **probKeyBuffer, struct alphParams *apr, char *filename) {
  FILE *fp;
  if((fp = fopen((char *) filename, "w")) == NULL) {
    printf("Can't create file\n");
    return;
  }

  register uint16_t i, j;
  for(i = 0; i < apr->alphS; ++i) {
    for(j = 0; j < (*probKeyBuffer)[i].size; ++j) {
    fprintf(fp, "%c%c", (((*probKeyBuffer)[i].buffer[j]) >> 8), (uint8_t)(*probKeyBuffer)[i].buffer[j]);
    }
    fprintf(fp, "\n"); 
  }
} 

#ifdef DEBUG_
void test() {
  printf("[TEST] start\n");
  printf("cipherUTF8 size = %d\n", cipherUTF8.size);
  printf("cipherdata size = %d\n", cipherdata.size);
  printf("cmpBuffer size = %d\n", cmpBuffer.size);
  printf("r = %f\n", r_);
  printf("rt = %f\n", rt_);
  printf("keyWordFrequency size = %d\n", keyWordFrequency.size);
  printf("probKeyBuffer[0] size = %d\n", probKeyBuffer[0].size);
  printf("decdata size = %d\n", decdata.size);
  printf("ksize = %d\n", ksize);
  printf("keyUTF8 size = %d\n", keyUTF8.size);
  printf("keydata size = %d\n", keydata.size);
  printf("decdataUTF8 size = %d\n", outdataUTF8.size);
  printf("keySizeBufferQuant size = %d\n", keySizeBufferQuant);
  printf("[TEST] end\n");
}
#endif

#ifdef DEBUG_
void printAlphParams(struct alphParams *apr) {
  printf("struct alph params ----------- \n");
  printf("low limit %02X\n", apr->aLiml);
  printf("high limit %02X\n", apr->aLimh);
  printf("alph start %04X\n", apr->aStart);
  printf("alph end %04X\n", apr->aEnd);
  printf("alph ad %04X\n", apr->aAd);
  printf("alph size %02X\n", apr->alphS);
  printf("alphabetL is %s\n", (apr->alphL == alphRu33) ? ARGRU33 : (apr->alphL == alphRu32) 
                           ? ARGRU32 : 
                             (apr->alphL == alphEn26) ? ARGEN26 : "[Error] alphL\0");

  printf("alphabetF is %s\n", (apr->alphF == freqOfAlphRu33) ? ARGRU33 : (apr->alphF == freqOfAlphRu32) 
                           ? ARGRU32 : 
                           (apr->alphF == freqOfAlphEn26) ? ARGEN26 : "[Error] alphF\0");
  printf(" ----------------------------- \n");
}
#endif

char setAlphabet(struct alphParams *apr, char *data) 
{
  if(cmpArgs(data, ARGRU33)) 
  { 
    apr->alphL = alphRu33; 
    apr->alphF = freqOfAlphRu33; 
    apr->alphS = 33;
    apr->aLiml = (uint8_t)(alphRu33[0].sym->utfcode >> 8);
    apr->aLimh = (uint8_t)(alphRu33[32].sym->utfcode >> 8);
    apr->aStart = alphRu33[0].sym->utfcode;
    apr->aEnd = alphRu33[32].sym->utfcode;
    apr->aAd = alphRu33[6].sym->utfcode;
  } 
  else if(cmpArgs(data, ARGRU32)) 
  { 
    apr->alphL = alphRu32; 
    apr->alphF = freqOfAlphRu32;
    apr->alphS = 32;
    apr->aLiml = (uint8_t)(alphRu32[0].sym->utfcode >> 8);
    apr->aLimh = (uint8_t)(alphRu32[31].sym->utfcode >> 8);
    apr->aStart = alphRu32[0].sym->utfcode;
    apr->aEnd = alphRu32[31].sym->utfcode;
    apr->aAd = 0;
  } 
  else if(cmpArgs(data, ARGEN26)) 
  { 
    apr->alphL = alphEn26; 
    apr->alphF = freqOfAlphEn26; 
    apr->alphS = 26; 
    apr->aLiml = (uint8_t)(alphEn26[0].sym->utfcode);
    apr->aLimh = (uint8_t)(alphEn26[25].sym->utfcode);
    apr->aStart = alphEn26[0].sym->utfcode;
    apr->aEnd = alphEn26[25].sym->utfcode;
    apr->aAd = 0;
  } 
  else { 
    printf("alph not found\n");
    return 0;
  }
  return 1;
}

void cpAlphRuDataToUTF8(uint8_t *bufferIn, uint16_t sizeIn, 
                         struct buffer16 *var, struct alphParams *apr) {
  register uint16_t i, n;

  for(i = 0, n = 0; i < sizeIn - 1; ++i) {
    if(bufferIn[i] == apr->aLiml || bufferIn[i] == apr->aLimh) {

      var->buffer[n] = (uint16_t)((bufferIn[i] << 8) | bufferIn[i + 1]);

      if((apr->aStart <= var->buffer[n]
          && apr->aEnd >= var->buffer[n]) 
          || apr->aAd == var->buffer[n]) 
      {
        if(n < var->size) { printf("das"); ++n; ++i; }
        else { 
          i = sizeIn;
#ifdef DEBUG_
          printf("[Error] fix me\n"); 
#endif
          break; 
          
        }
        printf("bad\n");

      } else {
        var->buffer[n] ^= var->buffer[n];
      }

    }
  }
  var->size = n;
}

void cpAlphEnDataToUTF8(uint8_t *bufferIn, uint16_t sizeIn, 
                         struct buffer16 *var, struct alphParams *apr) {
  register uint16_t i, n;

  for(i = 0, n = 0; i < sizeIn - 1; ++i) {

      var->buffer[n] = bufferIn[i];

      if(apr->aStart <= var->buffer[n] && apr->aEnd >= var->buffer[n]) 
      {
        if(n < var->size) { ++n; ++i; }
        else { 
          i = sizeIn; 
#ifdef DEBUG_
          printf("[Error] fix me\n"); 
#endif
          break;
        }

      } else {
        var->buffer[n] ^= var->buffer[n];
      }
  }
  var->size = n;
}

void setRT(double *rt_, char *data) {
  *rt_ = strtod(data, NULL);
}

void setKeySizeIn(uint8_t *ksizein, char *data) {
  *ksizein = atoi(data);
}

void interpretator(char *comand, char *data) {
  if(cmpArgs(comand, FILECIPHERIN)) {

#ifdef DEBUG_ 
    printf("[FILECIPHER] start\n");
#endif

    readFile(data);
    buffer16Alloc0d(&cipherUTF8, fsize);
    printf("debug fsize %d\n", fsize);

    printf("deb ciphsize %d\n", cipherUTF8.size);
    if(alphpr.alphL == alphRu33 || alphpr.alphL == alphRu32) {
      cpAlphRuDataToUTF8(fdata, fsize, &cipherUTF8, &alphpr);
    printf("deb ciphsize %d\n", cipherUTF8.size);
    } else if(alphpr.alphL == alphEn26) {
      cpAlphEnDataToUTF8(fdata, fsize, &cipherUTF8, &alphpr);
    } else {
      printf("[ERROR] alphpr\n");
      return;
    }
#ifdef DEBUG_
    printf("[FILECIPHER] end\n");
#endif
  }

  if(cmpArgs(comand, FILEKEYIN)) {
#ifdef DEBUG_
    printf("[FILEKEYIN] start\n");
#endif
    
    readFileToBuffer8(&kdata, &ksize, data);
    buffer16Alloc0d(&keyUTF8, ksize);

    if(alphpr.alphL == alphRu33 || alphpr.alphL == alphRu32) {
      cpAlphRuDataToUTF8(kdata, ksize, &keyUTF8, &alphpr);
    } else if(alphpr.alphL == alphEn26) {
      cpAlphEnDataToUTF8(kdata, ksize, &keyUTF8, &alphpr);
    } else {
#ifdef DEBUG_
      printf("[ERROR] alphpr\n");
#endif
      return;
    }

    buffer8Alloc0d(&keydata, keyUTF8.size);
    if(!cpDataFrom16To8(&keyUTF8, &keydata)) {
      printf("cp keydata failing\n");
    }
#ifdef DEBUG_
    printf("[FILEKEYIN] end\n");
#endif
  }

  if(cmpArgs(comand, FILECIPHEROUT)) {
#ifdef DEBUG_
    printf("[FILECIPHEROUT] start\n");
#endif
    writeFile((uint8_t *)cipherUTF8.buffer, cipherUTF8.size, data);
    printf("debug %d\n", cipherUTF8.size);
#ifdef DEBUG_
    printf("[FILECIPHEROUT] end\n");
#endif
  }

  if(cmpArgs(comand, FILEKEYOUT)) {
#ifdef DEBUG_
    printf("[FILEKEYOUT] start\n");
#endif
    
    buffer8Alloc0d(&cipherdata, cipherUTF8.size);
    if(!cpDataFrom16To8(&cipherUTF8, &cipherdata)) {
      printf("cp keydata failing\n");
      return;
    }

    if(!ksizein) {
#ifdef DEBUG_
      printf("[ERROR] keysizein %d\n", ksizein);
#endif
      return;
    }

    buffer8Alloc0d(&keyWordFrequency, ksizein);
    findKeySymFreq(&cipherdata, &keyWordFrequency);

    buffer16Alloc1d(&probKeyBuffer, alphpr.alphS, keyWordFrequency.size);
    
    computeProbKeys(&probKeyBuffer, &keyWordFrequency, &alphpr);
    
    writeFileProbKeyBuffer(&probKeyBuffer, &alphpr, data); 
#ifdef DEBUG_
    printf("[FILEKEYOUT] end\n");
#endif
  }

  if(cmpArgs(comand, KEYSIZEIN)) {
#ifdef DEBUG_
    printf("[KEYSIZEIN] start\n");
#endif
    setKeySizeIn(&ksizein, data);
#ifdef DEBUG_
    printf("[KEYSIZEIN] end\n");
#endif
  }

  if(cmpArgs(comand, FILEKEYSIZEOUT)) {
#ifdef DEBUG_
    printf("[FILEKEYSIZEOUT] start\n");
#endif
    
    buffer8Alloc0d(&cipherdata, cipherUTF8.size);
    if(!cpDataFrom16To8(&cipherUTF8, &cipherdata)) {
      printf("cp cipherdata failing\n");
      return;
    }

    buffer16Alloc0d(&cmpBuffer, cipherdata.size);
    findKeySize(&cipherdata, &cmpBuffer);
    buffer8Alloc0d(&keySizeBuffer, keySizeBufferQuant);

    if(!findKeySizeUseFilter(&r_, &rt_, &cmpBuffer, &keySizeBuffer)) {
      printf("key size not exist\n");
    }

    writeFileKeySize(&keySizeBuffer, data);
#ifdef DEBUG_
    printf("[FILEKEYSIZEOUT] end\n");
#endif
  }

  if(cmpArgs(comand, ALPHVAR)) {
#ifdef DEBUG_
    printf("[ALPHVAR] start\n");
#endif
    setAlphabet(&alphpr, data);
#ifdef DEBUG_
    printf("[ALPHVAR] end\n");
#endif
  }

  if(cmpArgs(comand, FILEDEC)) {
#ifdef DEBUG_
    printf("[FILEDEC] start\n");
#endif

    buffer8Alloc0d(&cipherdata, cipherUTF8.size);
    if(!cpDataFrom16To8(&cipherUTF8, &cipherdata)) {
      printf("cp cipherdata failing\n");
      return;
    }

    buffer8Alloc0d(&decdata, cipherdata.size);
    decv2(&cipherdata, &decdata, &keydata);
    buffer16Alloc0d(&outdataUTF8, decdata.size);
    translateDataToOutUTF8(&alphpr, &decdata, &outdataUTF8);

    writeFileFromBuffer8((uint8_t *) outdataUTF8.buffer, 
        outdataUTF8.size, data);
#ifdef DEBUG_
    printf("[FILEDEC] end\n");
#endif
  }

  if(cmpArgs(comand, SETRT)) {
#ifdef DEBUG_
    printf("[RT] start\n");
#endif
    setRT(&rt_, data);
#ifdef DEBUG_
    printf("[RT] end\n");
#endif
  }

  if(cmpArgs(comand, HELP)) {
#ifdef DEBUG_
    printf("[HELP] start\n");
#endif

#ifdef DEBUG_
    printf("[HELP] end\n");
#endif
  }
}
