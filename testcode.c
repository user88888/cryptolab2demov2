#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "buffer.h"
#include "alph.h"
#include "file.h"

#define ARGRU33 "alphRu33\0"
#define ARGRU32 "alphRu32\0"
#define ARGEN26 "alphEn26\0"

struct alphParams {
  uint8_t aLiml;
  uint8_t aLimh;
  uint16_t aStart;
  uint16_t aEnd;
  uint16_t aAd;
  uint8_t alphS;
  struct alphabet *alphL;
  struct alphFreq *alphF;
};

struct alphParams apr;

void printAlphParams(struct alphParams *apr) {
  printf("struct alph params ----------- \n");
  printf("low limit %02X\n", apr->aLiml);
  printf("high limit %02X\n", apr->aLimh);
  printf("alph start %04X\n", apr->aStart);
  printf("alph end %04X\n", apr->aEnd);
  printf("alph ad %04X\n", apr->aAd);
  printf("alph size %02X\n", apr->alphS);
  printf("alphabetL is %s\n", (apr->alphL == alphRu33) ? ARGRU33 : (apr->alphL == alphRu32) ? ARGRU32 : 
                           (apr->alphL == alphEn26) ? ARGEN26 : "[Error] alphL\0");
  printf("alphabetF is %s\n", (apr->alphF == freqOfAlphRu33) ? ARGRU33 : (apr->alphF == freqOfAlphRu32) ? ARGRU32 : 
                           (apr->alphF == freqOfAlphEn26) ? ARGEN26 : "[Error] alphF\0");
  printf(" ----------------------------- \n");
}

char setAlphabet(struct alphParams *apr, char *data) 
{
  if(cmpArgs(data, ARGRU33)) 
  { 
    apr->alphL = alphRu33; 
    apr->alphF = freqOfAlphRu33; 
    apr->alphS = 33;
    apr->aLiml = (uint8_t)(alphRu33[0].sym->utfcode >> 8);
    apr->aLimh = (uint8_t)(alphRu33[32].sym->utfcode >> 8);
    apr->aStart = alphRu33[0].sym->utfcode;
    apr->aEnd = alphRu33[32].sym->utfcode;
    apr->aAd = alphRu33[6].sym->utfcode;
  } 
  else if(cmpArgs(data, ARGRU32)) 
  { 
    apr->alphL = alphRu32; 
    apr->alphF = freqOfAlphRu32;
    apr->alphS = 32;
    apr->aLiml = (uint8_t)(alphRu32[0].sym->utfcode >> 8);
    apr->aLimh = (uint8_t)(alphRu32[31].sym->utfcode >> 8);
    apr->aStart = alphRu32[0].sym->utfcode;
    apr->aEnd = alphRu32[31].sym->utfcode;
    apr->aAd = 0;
  } 
  else if(cmpArgs(data, ARGEN26)) 
  { 
    apr->alphL = alphEn26; 
    apr->alphF = freqOfAlphEn26; 
    apr->alphS = 26; 
    apr->aLiml = (uint8_t)(alphEn26[0].sym->utfcode);
    apr->aLimh = (uint8_t)(alphEn26[25].sym->utfcode);
    apr->aStart = alphEn26[0].sym->utfcode;
    apr->aEnd = alphEn26[25].sym->utfcode;
    apr->aAd = 0;
  } 
  else { 
    printf("alph not found\n");
    return 0;
  }
  return 1;
}

void cpAlphRuDataToUTF8(uint8_t *bufferIn, uint16_t sizeIn, 
                         struct buffer16 *var, struct alphParams *apr) {
  register uint16_t i, n;

  for(i = 0, n = 0; i < sizeIn - 1; ++i) {
    if(bufferIn[i] == apr->aLiml || bufferIn[i] == apr->aLimh) {

      var->buffer[n] = (uint16_t)((bufferIn[i] << 8) | bufferIn[i + 1]);

      if((apr->aStart <= var->buffer[n]
          && apr->aEnd >= var->buffer[n]) 
          || apr->aAd == var->buffer[n]) 
      {
        if(n < var->size) { ++n; ++i; }
        else { i = sizeIn; printf("[Error] fix me\n"); break; }

      } else {
        var->buffer[n] ^= var->buffer[n];
      }

    }
  }
  var->size = n;
}

void cpAlphEnDataToUTF8(uint8_t *bufferIn, uint16_t sizeIn, 
                         struct buffer16 *var, struct alphParams *apr) {
  register uint16_t i, n;

  for(i = 0, n = 0; i < sizeIn - 1; ++i) {

      var->buffer[n] = bufferIn[i];

      if(apr->aStart <= var->buffer[n] && apr->aEnd >= var->buffer[n]) 
      {
        if(n < var->size) { ++n; ++i; }
        else { i = sizeIn; printf("[Error] fix me\n"); break; }

      } else {
        var->buffer[n] ^= var->buffer[n];
      }
  }
  var->size = n;
}
