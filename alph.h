#define once
#include <stdint.h>

#define ALPHRU33 33
#define ALPHRU32 32
#define ALPHEN26 26

#define ARGRU33 "alphRu33\0"
#define ARGRU32 "alphRu32\0"
#define ARGEN26 "alphEn26\0"

struct symCode {
  wchar_t ch;
  uint16_t utfcode;
};

struct alphabet {
  struct symCode *sym;
  uint16_t number;
};

struct alphFreq {
  struct alphabet *letterPtr;
};

struct alphParams {
  uint8_t aLiml;
  uint8_t aLimh;
  uint16_t aStart;
  uint16_t aEnd;
  uint16_t aAd;
  uint8_t alphS;
  struct alphabet *alphL;
  struct alphFreq *alphF;
};

static struct symCode L1 = { .ch = L'а', .utfcode = 0xD0B0 };
static struct symCode L2 = { .ch = L'б', .utfcode = 0xD0B1 };
static struct symCode L3 = { .ch = L'в', .utfcode = 0xD0B2 };
static struct symCode L4 = { .ch = L'г', .utfcode = 0xD0B3 };
static struct symCode L5 = { .ch = L'д', .utfcode = 0xD0B4 };
static struct symCode L6 = { .ch = L'е', .utfcode = 0xD0B5 };
static struct symCode L7 = { .ch = L'ё', .utfcode = 0xD191 };
static struct symCode L8 = { .ch = L'ж', .utfcode = 0xD0B6 };
static struct symCode L9 = { .ch = L'з', .utfcode = 0xD0B7 };
static struct symCode L10 = { .ch = L'и', .utfcode = 0xD0B8 };
static struct symCode L11 = { .ch = L'й', .utfcode = 0xD0B9 };
static struct symCode L12 = { .ch = L'к', .utfcode = 0xD0BA };
static struct symCode L13 = { .ch = L'л', .utfcode = 0xD0BB };
static struct symCode L14 = { .ch = L'м', .utfcode = 0xD0BC };
static struct symCode L15 = { .ch = L'н', .utfcode = 0xD0BD };
static struct symCode L16 = { .ch = L'о', .utfcode = 0xD0BE };
static struct symCode L17 = { .ch = L'п', .utfcode = 0xD0BF };
static struct symCode L18 = { .ch = L'р', .utfcode = 0xD180 };
static struct symCode L19 = { .ch = L'с', .utfcode = 0xD181 };
static struct symCode L20 = { .ch = L'т', .utfcode = 0xD182 };
static struct symCode L21 = { .ch = L'у', .utfcode = 0xD183 };
static struct symCode L22 = { .ch = L'ф', .utfcode = 0xD184 };
static struct symCode L23 = { .ch = L'х', .utfcode = 0xD185 };
static struct symCode L24 = { .ch = L'ц', .utfcode = 0xD186 };
static struct symCode L25 = { .ch = L'ч', .utfcode = 0xD187 };
static struct symCode L26 = { .ch = L'ш', .utfcode = 0xD188 };
static struct symCode L27 = { .ch = L'щ', .utfcode = 0xD189 };
static struct symCode L28 = { .ch = L'ъ', .utfcode = 0xD18A };
static struct symCode L29 = { .ch = L'ы', .utfcode = 0xD18B };
static struct symCode L30 = { .ch = L'ь', .utfcode = 0xD18C };
static struct symCode L31 = { .ch = L'э', .utfcode = 0xD18D };
static struct symCode L32 = { .ch = L'ю', .utfcode = 0xD18E };
static struct symCode L33 = { .ch = L'я', .utfcode = 0xD18F };

static struct symCode E1 = { .ch = L'a', .utfcode = 0x0061 };
static struct symCode E2 = { .ch = L'b', .utfcode = 0x0062 };
static struct symCode E3 = { .ch = L'c', .utfcode = 0x0063 };
static struct symCode E4 = { .ch = L'd', .utfcode = 0x0064 };
static struct symCode E5 = { .ch = L'e', .utfcode = 0x0065 };
static struct symCode E6 = { .ch = L'f', .utfcode = 0x0066 };
static struct symCode E7 = { .ch = L'g', .utfcode = 0x0067 };
static struct symCode E8 = { .ch = L'h', .utfcode = 0x0068 };
static struct symCode E9 = { .ch = L'i', .utfcode = 0x0069 };
static struct symCode E10 = { .ch = L'j', .utfcode = 0x006A };
static struct symCode E11 = { .ch = L'k', .utfcode = 0x006B };
static struct symCode E12 = { .ch = L'l', .utfcode = 0x006C };
static struct symCode E13 = { .ch = L'm', .utfcode = 0x006D };
static struct symCode E14 = { .ch = L'n', .utfcode = 0x006E };
static struct symCode E15 = { .ch = L'o', .utfcode = 0x006F };
static struct symCode E16 = { .ch = L'p', .utfcode = 0x0070 };
static struct symCode E17 = { .ch = L'q', .utfcode = 0x0071 };
static struct symCode E18 = { .ch = L'r', .utfcode = 0x0072 };
static struct symCode E19 = { .ch = L's', .utfcode = 0x0073 };
static struct symCode E20 = { .ch = L't', .utfcode = 0x0074 };
static struct symCode E21 = { .ch = L'u', .utfcode = 0x0075 };
static struct symCode E22 = { .ch = L'v', .utfcode = 0x0076 };
static struct symCode E23 = { .ch = L'w', .utfcode = 0x0077 };
static struct symCode E24 = { .ch = L'x', .utfcode = 0x0078 };
static struct symCode E25 = { .ch = L'y', .utfcode = 0x0079 };
static struct symCode E26 = { .ch = L'z', .utfcode = 0x007A };

struct alphabet alphRu33[33] = {
  { .sym = &L1, .number = 1 },
	{ .sym = &L2, .number = 2 },
	{ .sym = &L3, .number = 3 },
	{ .sym = &L4, .number = 4 },
	{ .sym = &L5, .number = 5 },
	{ .sym = &L6, .number = 6 },
	{ .sym = &L7, .number = 7 },
	{ .sym = &L8, .number = 8 },
	{ .sym = &L9, .number = 9 },
	{ .sym = &L10, .number = 10 },
	{ .sym = &L11, .number = 11 },
	{ .sym = &L12, .number = 12 },
	{ .sym = &L13, .number = 13 },
	{ .sym = &L14, .number = 14 },
	{ .sym = &L15, .number = 15 },
	{ .sym = &L16, .number = 16 },
	{ .sym = &L17, .number = 17 },
	{ .sym = &L18, .number = 18 },
	{ .sym = &L19, .number = 19 },
	{ .sym = &L20, .number = 20 },
	{ .sym = &L21, .number = 21 },
	{ .sym = &L22, .number = 22 },
	{ .sym = &L23, .number = 23 },
	{ .sym = &L24, .number = 24 },
	{ .sym = &L25, .number = 25 },
	{ .sym = &L26, .number = 26 },
	{ .sym = &L27, .number = 27 },
	{ .sym = &L28, .number = 28 },
	{ .sym = &L29, .number = 29 },
	{ .sym = &L30, .number = 30 },
	{ .sym = &L31, .number = 31 },
	{ .sym = &L32, .number = 32 },
	{ .sym = &L33, .number = 33 }
};

struct alphabet alphRu32[32] = {
  { .sym = &L1, .number = 1 },
  { .sym = &L2, .number = 2 },
  { .sym = &L3, .number = 3 },
  { .sym = &L4, .number = 4 },
  { .sym = &L5, .number = 5 },
  { .sym = &L6, .number = 6 },
  { .sym = &L8, .number = 7 },
  { .sym = &L9, .number = 8 },
  { .sym = &L10, .number = 9 },
  { .sym = &L11, .number = 10 },
  { .sym = &L12, .number = 11 },
  { .sym = &L13, .number = 12 },
  { .sym = &L14, .number = 13 },
  { .sym = &L15, .number = 14 },
  { .sym = &L16, .number = 15 },
  { .sym = &L17, .number = 16 },
  { .sym = &L18, .number = 17 },
  { .sym = &L19, .number = 18 },
  { .sym = &L20, .number = 19 },
  { .sym = &L21, .number = 20 },
  { .sym = &L22, .number = 21 },
  { .sym = &L23, .number = 22 },
  { .sym = &L24, .number = 23 },
  { .sym = &L25, .number = 24 },
  { .sym = &L26, .number = 25 },
  { .sym = &L27, .number = 26 },
  { .sym = &L28, .number = 27 },
  { .sym = &L29, .number = 28 },
  { .sym = &L30, .number = 29 },
  { .sym = &L31, .number = 30 },
  { .sym = &L32, .number = 31 },
  { .sym = &L33, .number = 32 }	
};

struct alphabet alphEn26[26] =  {
  { .sym = &E1, .number = 1 },
	{ .sym = &E2, .number = 2 },
	{ .sym = &E3, .number = 3 },
	{ .sym = &E4, .number = 4 },
	{ .sym = &E5, .number = 5 },
	{ .sym = &E6, .number = 6 },
	{ .sym = &E7, .number = 7 },
	{ .sym = &E8, .number = 8 },
	{ .sym = &E9, .number = 9 },
	{ .sym = &E10, .number = 10 },
	{ .sym = &E11, .number = 11 },
	{ .sym = &E12, .number = 12 },
	{ .sym = &E13, .number = 13 },
	{ .sym = &E14, .number = 14 },
	{ .sym = &E15, .number = 15 },
	{ .sym = &E16, .number = 16 },
	{ .sym = &E17, .number = 17 },
	{ .sym = &E18, .number = 18 },
	{ .sym = &E19, .number = 19 },
	{ .sym = &E20, .number = 20 },
	{ .sym = &E21, .number = 21 },
	{ .sym = &E22, .number = 22 },
	{ .sym = &E23, .number = 23 },
	{ .sym = &E24, .number = 24 },
	{ .sym = &E25, .number = 25 },
	{ .sym = &E26, .number = 26 }
};

struct alphFreq freqOfAlphRu33[33] = {
  {.letterPtr = &alphRu33[15]},
  {.letterPtr = &alphRu33[5]},
  {.letterPtr = &alphRu33[0]},
  {.letterPtr = &alphRu33[9]},
  {.letterPtr = &alphRu33[14]},
  {.letterPtr = &alphRu33[19]},
  {.letterPtr = &alphRu33[18]},
  {.letterPtr = &alphRu33[17]},
  {.letterPtr = &alphRu33[2]},
  {.letterPtr = &alphRu33[12]},
  {.letterPtr = &alphRu33[11]},
  {.letterPtr = &alphRu33[13]},
  {.letterPtr = &alphRu33[4]},
  {.letterPtr = &alphRu33[16]},
  {.letterPtr = &alphRu33[20]},
  {.letterPtr = &alphRu33[32]},
  {.letterPtr = &alphRu33[28]},
  {.letterPtr = &alphRu33[29]},
  {.letterPtr = &alphRu33[3]},
  {.letterPtr = &alphRu33[8]},
  {.letterPtr = &alphRu33[1]},
  {.letterPtr = &alphRu33[24]},
  {.letterPtr = &alphRu33[10]},
  {.letterPtr = &alphRu33[22]},
  {.letterPtr = &alphRu33[7]},
  {.letterPtr = &alphRu33[25]},
  {.letterPtr = &alphRu33[31]},
  {.letterPtr = &alphRu33[23]},
  {.letterPtr = &alphRu33[26]},
  {.letterPtr = &alphRu33[30]},
  {.letterPtr = &alphRu33[21]},
  {.letterPtr = &alphRu33[27]},
  {.letterPtr = &alphRu33[6]}
};

struct alphFreq freqOfAlphRu32[32] = {
  {.letterPtr = &alphRu32[14]},
  {.letterPtr = &alphRu32[5]},
  {.letterPtr = &alphRu32[0]},
  {.letterPtr = &alphRu32[8]},
  {.letterPtr = &alphRu32[13]},
  {.letterPtr = &alphRu32[18]},
  {.letterPtr = &alphRu32[17]},
  {.letterPtr = &alphRu32[16]},
  {.letterPtr = &alphRu32[2]},
  {.letterPtr = &alphRu32[11]},
  {.letterPtr = &alphRu32[10]},
  {.letterPtr = &alphRu32[12]},
  {.letterPtr = &alphRu32[4]},
  {.letterPtr = &alphRu32[15]},
  {.letterPtr = &alphRu32[19]},
  {.letterPtr = &alphRu32[31]},
  {.letterPtr = &alphRu32[27]},
  {.letterPtr = &alphRu32[28]},
  {.letterPtr = &alphRu32[3]},
  {.letterPtr = &alphRu32[7]},
  {.letterPtr = &alphRu32[1]},
  {.letterPtr = &alphRu32[23]},
  {.letterPtr = &alphRu32[9]},
  {.letterPtr = &alphRu32[21]},
  {.letterPtr = &alphRu32[6]},
  {.letterPtr = &alphRu32[24]},
  {.letterPtr = &alphRu32[30]},
  {.letterPtr = &alphRu32[22]},
  {.letterPtr = &alphRu32[25]},
  {.letterPtr = &alphRu32[29]},
  {.letterPtr = &alphRu32[20]},
  {.letterPtr = &alphRu32[26]}
};

struct alphFreq freqOfAlphEn26[26] = {
  {.letterPtr = &alphEn26[4]},
  {.letterPtr = &alphEn26[19]},
  {.letterPtr = &alphEn26[0]},
  {.letterPtr = &alphEn26[14]},
  {.letterPtr = &alphEn26[8]},
  {.letterPtr = &alphEn26[13]},
  {.letterPtr = &alphEn26[18]},
  {.letterPtr = &alphEn26[7]},
  {.letterPtr = &alphEn26[17]},
  {.letterPtr = &alphEn26[3]},
  {.letterPtr = &alphEn26[11]},
  {.letterPtr = &alphEn26[2]},
  {.letterPtr = &alphEn26[20]},
  {.letterPtr = &alphEn26[12]},
  {.letterPtr = &alphEn26[22]},
  {.letterPtr = &alphEn26[5]},
  {.letterPtr = &alphEn26[6]},
  {.letterPtr = &alphEn26[24]},
  {.letterPtr = &alphEn26[15]},
  {.letterPtr = &alphEn26[1]},
  {.letterPtr = &alphEn26[21]},
  {.letterPtr = &alphEn26[10]},
  {.letterPtr = &alphEn26[9]},
  {.letterPtr = &alphEn26[23]},
  {.letterPtr = &alphEn26[16]},
  {.letterPtr = &alphEn26[25]},
};

uint8_t alphDecRu33[33][33] = {
{ 0, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1  },
{ 1, 0, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2  },
{ 2, 1, 0, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3  },
{ 3, 2, 1, 0, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4  },
{ 4, 3, 2, 1, 0, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5  },
{ 5, 4, 3, 2, 1, 0, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6  },
{ 6, 5, 4, 3, 2, 1, 0, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7  },
{ 7, 6, 5, 4, 3, 2, 1, 0, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8  },
{ 8, 7, 6, 5, 4, 3, 2, 1, 0, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9  },
{ 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10  },
{ 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11  },
{ 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12  },
{ 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13  },
{ 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14  },
{ 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15  },
{ 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16  },
{ 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17  },
{ 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18  },
{ 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19  },
{ 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20  },
{ 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21  },
{ 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22  },
{ 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23  },
{ 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 32, 31, 30, 29, 28, 27, 26, 25, 24  },
{ 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 32, 31, 30, 29, 28, 27, 26, 25  },
{ 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 32, 31, 30, 29, 28, 27, 26  },
{ 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 32, 31, 30, 29, 28, 27  },
{ 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 32, 31, 30, 29, 28  },
{ 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 32, 31, 30, 29  },
{ 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 32, 31, 30  },
{ 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 32, 31  },
{ 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 32  },
{ 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0  }
};

uint8_t alphDecRu32[32][32] = {
{ 0, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1  },
{ 1, 0, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2  },
{ 2, 1, 0, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3  },
{ 3, 2, 1, 0, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4  },
{ 4, 3, 2, 1, 0, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5  },
{ 5, 4, 3, 2, 1, 0, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6  },
{ 6, 5, 4, 3, 2, 1, 0, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7  },
{ 7, 6, 5, 4, 3, 2, 1, 0, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8  },
{ 8, 7, 6, 5, 4, 3, 2, 1, 0, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9  },
{ 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10  },
{ 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11  },
{ 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12  },
{ 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13  },
{ 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14  },
{ 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15  },
{ 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16  },
{ 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17  },
{ 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18  },
{ 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19  },
{ 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20  },
{ 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21  },
{ 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22  },
{ 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 31, 30, 29, 28, 27, 26, 25, 24, 23  },
{ 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 31, 30, 29, 28, 27, 26, 25, 24  },
{ 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 31, 30, 29, 28, 27, 26, 25  },
{ 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 31, 30, 29, 28, 27, 26  },
{ 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 31, 30, 29, 28, 27  },
{ 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 31, 30, 29, 28  },
{ 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 31, 30, 29  },
{ 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 31, 30  },
{ 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 31  },
{ 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0  }
};

uint8_t alphDecEn26[26][26] = {
{ 0, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1  },
{ 1, 0, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2  },
{ 2, 1, 0, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3  },
{ 3, 2, 1, 0, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4  },
{ 4, 3, 2, 1, 0, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5  },
{ 5, 4, 3, 2, 1, 0, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6  },
{ 6, 5, 4, 3, 2, 1, 0, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7  },
{ 7, 6, 5, 4, 3, 2, 1, 0, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8  },
{ 8, 7, 6, 5, 4, 3, 2, 1, 0, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9  },
{ 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10  },
{ 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11  },
{ 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12  },
{ 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13  },
{ 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14  },
{ 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15  },
{ 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16  },
{ 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 25, 24, 23, 22, 21, 20, 19, 18, 17  },
{ 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 25, 24, 23, 22, 21, 20, 19, 18  },
{ 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 25, 24, 23, 22, 21, 20, 19  },
{ 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 25, 24, 23, 22, 21, 20  },
{ 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 25, 24, 23, 22, 21  },
{ 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 25, 24, 23, 22  },
{ 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 25, 24, 23  },
{ 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 25, 24  },
{ 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 25  },
{ 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0  }
};

uint16_t typeSym(uint16_t sym) {
  register uint8_t i = 0;
  while(sym != alphRu33[i].sym->utfcode && ++i < 33);
  return  (i < 33) ? alphRu33[i].sym->ch : 0;
}

uint16_t searchNumRu33(uint16_t sym) {
  register uint8_t i = 0;
  while(((sym - alphRu33[i].sym->utfcode) != 0) && ++i < 33);
  return (i < 33) ? alphRu33[i].number : 0;
}

uint16_t searchNumRu32(uint16_t sym) {
  register uint8_t i = 0;
  while(((sym - alphRu32[i].sym->utfcode) != 0) && ++i < 32);
  return (i < 32) ? alphRu32[i].number : 0;
}

uint16_t alphSearchNum(uint16_t sym, uint16_t type) {
  switch(type) {
  case ALPHRU33:
    sym = searchNumRu33(sym);
    break;
  case ALPHRU32:
    sym = searchNumRu32(sym);
    break;
  default:
    return 0;
    break;
  }
  return sym;
}

uint16_t alphSearchSym(uint16_t num, uint16_t type) {
  switch(type) {
  case ALPHRU33: return (num < 33) ? alphRu33[num].sym->utfcode : 0; break;
  case ALPHRU32: return (num < 32) ? alphRu32[num].sym->utfcode : 0; break;
  default:
    return 0;
    break;
  }
}

uint8_t retAlph8BitNum(struct alphabet *alph,  uint16_t sym, uint8_t alphSize) {
  do {
    if(alph[--alphSize].sym->utfcode == sym) 
      return alph[alphSize].number - 1;
  } while(alphSize);
  
  return 0;
}
