#define once
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

uint16_t filelength(FILE *fp) {
  uint16_t length;
  fseek(fp, 0, SEEK_END);
  length = ftell(fp);
  fseek(fp, 0, SEEK_SET);
  return length;
}

char readFileToBuffer8(uint8_t **fdata, uint16_t *fsize, char *filename) {
  FILE *fp;
  if((fp = fopen((char *) filename, "r")) == NULL) {
    printf("Can't open file.\n");
    return 0;
  }

  *fsize = filelength(fp);
  *fdata = (uint8_t *)malloc(sizeof(uint8_t) * (*fsize));
  
  if(fdata == NULL) {
    printf("Bad alloc\n");
    return 0;
  }
  
  fread(*fdata, 1, *fsize, fp);
  fclose(fp);

  return 1;
}

void writeFileFromBuffer8(uint8_t *buffer, uint16_t size, char *filename) {
  FILE *fp;
  if((fp = fopen((char *) filename, "w")) == NULL) {
    printf("Can't create file.\n");
    return;
  }
  for(uint16_t i = 0; i < size - 1; i += 2) {
      fprintf(fp, "%c%c", buffer[i + 1], buffer[i]);
  }
  fclose(fp);
}

void writeFileFromBuffer16(uint16_t *buffer, uint16_t size, char *filename) {
  FILE *fp;
  if((fp = fopen((char *) filename, "w")) == NULL) {
    printf("Can't create file.\n");
    return;
  }
  fwrite(buffer, sizeof(uint16_t), size, fp);
  fclose(fp);
}

void writeFileTextBuffer(uint8_t *buffer, uint16_t size, char *filename) {
  FILE *fp;
  if((fp = fopen((char *) filename, "w")) == NULL) {
    printf("Can't create file.\n");
    return;
  }

  do {
    --size;
    fwrite(buffer, 1, 1, fp);
    ++buffer;
  } while(size);
  fclose(fp);
}

void writeFileKeySize(struct buffer8 *keyBuffer, char *filename) {
  FILE *fp;
  if((fp = fopen((char *) filename, "w")) == NULL) {
    printf("Can't create file.\n");
    return;
  }

  for(uint16_t i = 0; i < keyBuffer->size; ++i) {
    fprintf(fp, "%d\n", keyBuffer->buffer[i]);
  }
  fclose(fp);
}
